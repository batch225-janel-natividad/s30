s30 Activity Instructions:
1. Create an activity.js file on where to write and save the solution for the activity.
2. Use the count operator to count the total number of fruits on sale.
3. Use the count operator to count the total number of fruits with stock more than 20.
4. Use the average operator to get the average price of fruits onSale per supplier.
5. Use the max operator to get the highest price of a fruit per supplier.
6. Use the min operator to get the lowest price of a fruit per supplier.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
9. Add the link in Boodle.


//COUNT
     db.fruits.aggregate ([
   {
      $match: {
         onSale: true
      }
   },
   {
      $count: "fruits"
   }
])

//COUNT STOCK
     db.fruits.aggregate ([
   {
      $match: {
         onSale: true,
         stock: { $gte: 20 }
      }
   },
   {
      $count: "enoughStock"
   }
]);

//AVERAGE
db.fruits.aggregate([
    {
        $match: { onSale: true }
    },
    {
        $group: {
            _id: "$supplier_id",
            avg_price: {$sum: "$stock"}
        }
    }

]);

//MAX
db.fruits.aggregate([
    {
        $match: { onSale: true}
    },
    {
        $group: {
            _id: "$supplier_id",
             maxPrice: { $max: "$price" }
      }
    }

]);

//MIN
db.fruits.aggregate([
    {
        $match: { onSale: true}
    },
    {
        $group: {
            _id: "$supplier_id",
             minPrice: { $min: "$price" }
      }
    }

]);

